import { Component, OnInit } from '@angular/core';
import {HomeService} from '../services/home.service';
import { Site } from './site';
import { OwlOptions } from 'ngx-owl-carousel-o';
import swal from 'sweetalert2';
import {MatDialog} from '@angular/material/dialog';
import { MapaComponent } from '../mapa/mapa.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag:true,
    touchDrag: true,
    pullDrag: true,
    nav:false,
    dots: true,
    navSpeed: 700,  
    margin:10,
    navText:['<',">"], 
    responsive: {
      0: {
        items: 2,
        dots:false,
        nav:true 
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    }
  }
  sites:Site[];
  modalRef: BsModalRef;

  constructor(private service:HomeService,private modalService: BsModalService) {
    swal.fire('Cargando');
    swal.showLoading();
   
   }

  ngOnInit(): void {
    this.service.get().subscribe((r:any)=>{
      this.sites=r.sites;     
      swal.close();
    })
  }

 
  getPrincipales(){
    if(this.sites.length>5){
      return this.sites.filter(s=>this.sites.indexOf(s)<6);    
    }
    return this.sites;
  }

  getSecundarias(){
    if(this.sites.length>5){
      return this.sites.filter(s=>this.sites.indexOf(s)>5);
    }
    return [];
  }


  openDialog(site:Site) {
    this.modalRef = this.modalService.show(MapaComponent);
    this.modalRef.content.data=site;
    this.modalRef.content.iniciarMapa();
  }

}
