import {Ubicacion} from './ubicacion';

export class Site {
    descripcion: string;
    nombre: string;
    ubicacion: Ubicacion;
    url_imagen: string;
}
