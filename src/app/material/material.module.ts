import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule  } from '@angular/material/input';
import {  MatButtonModule} from '@angular/material/button';
import {  MatSelectModule } from '@angular/material/select';
import {  MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';

@NgModule({
  declarations: [],
  imports: [
    CommonModule, 
    FormsModule,    
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule,
    MatDialogModule,
    MatBottomSheetModule
  ],
  exports:[    
    FormsModule,    
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule,
    MatDialogModule,
    MatBottomSheetModule
  ]
    ,
    providers:[{ provide: MAT_DIALOG_DATA, useValue: {} },
      { provide: MatDialogRef, useValue: {} }]
})
export class MaterialModule { }
