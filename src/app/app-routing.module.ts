import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from './layout/layout.component';
import { AppComponent } from './app.component';
import {CanActivateToken} from './interceptors/can-activate-token';

const routes: Routes = [
  {path:'',redirectTo:"home",pathMatch:'full'},
  {path:'',component:AppComponent, children:[
   
    {path:'home',loadChildren:  './layout/layout.module#LayoutModule',canActivate:[CanActivateToken]},
    {path:'login',loadChildren:  './user/user.module#UserModule'}
  ]}
  ,{path:'**',redirectTo:"home",pathMatch:'full'}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
