import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
 url="auth";

  constructor(private http:HttpClient) { }

  login(user){
    let headers=new HttpHeaders().set("Content-Type","application/json");   
    return this.http.post(this.getUrl(),user,{headers});
  }

  getUrl():string{
   return environment.url+this.url;
  }
}
