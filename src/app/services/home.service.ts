import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import {Site} from '../home/site';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  url="sites";

  constructor(private http:HttpClient) { }

 get(){   
    return this.http.get(this.getUrl());
  }

  getUrl():string{
   return environment.url+this.url;
  }
}
