export class AppStorage {
  static logout() {
    if(AppStorage.isToken()){
      sessionStorage.removeItem('token');
    }   
  }
 
 
  static setToken(token:string) {
   sessionStorage.setItem('token',token);
  }

  static getToken(): string {
   return sessionStorage.getItem('token');
  }

  static isToken():boolean{
    return sessionStorage.getItem('token')!=null;
}
}
