import { Component, OnInit, Input, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as mapboxgl from 'mapbox-gl';
import { Site } from '../home/site';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HomeComponent } from '../home/home.component';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

data;
mapa:mapboxgl;
  constructor(public modalRef:BsModalRef) { }

  ngOnInit(): void {
    mapboxgl.accessToken = environment.mapBoxKey;
    this.mapa = new mapboxgl.Map({
      container: 'mapa-mapbox', // container id
      style: 'mapbox://styles/mapbox/streets-v11',      
      zoom: 15 // starting zoom
      });
   
  }

  iniciarMapa(){
    this.mapa = new mapboxgl.Map({
      container: 'mapa-mapbox', // container id
      style: 'mapbox://styles/mapbox/streets-v11',      
      zoom: 15 // starting zoom
      ,center:([this.data.ubicacion._long, this.data.ubicacion._lat])
      });  
   var popup = new mapboxgl.Popup({ closeOnClick: false })
   .setLngLat([this.data.ubicacion._long, this.data.ubicacion._lat])
   .setHTML(this.data.nombre)
   .addTo(this.mapa);
  }

}
