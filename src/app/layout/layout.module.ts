import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { HomeComponent } from '../home/home.component';
import { LayoutComponent } from './layout.component';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { HeaderComponent } from '../header/header.component';
import { MapaComponent } from '../mapa/mapa.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [HomeComponent, LayoutComponent,HeaderComponent,
    MapaComponent],
  imports: [
    CommonModule,      
    LayoutRoutingModule,
    MaterialModule,
    CarouselModule,
    ModalModule.forRoot()
  ]
})
export class LayoutModule { }
