import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {LoginService} from '../services/login.service';
import {AppStorage} from '../app-storage';
import { Router } from '@angular/router';
import swal from'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],  
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  user={email:"",password:""};
  
  constructor(private service:LoginService,private router:Router){
    AppStorage.logout();
  }
  
  ngOnInit(): void {   
  }

  login(){
    swal.fire('Cargando');
    swal.showLoading();
    this.service.login(this.user).subscribe((resp:string)=>{
      AppStorage.setToken(resp);
      this.router.navigate(['home']);
      swal.close();
    },(error:HttpErrorResponse)=>{  
      swal.close();
      swal.fire('Error',error.message,'error');
    })
  }
 

}
