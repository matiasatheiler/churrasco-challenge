import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from '../login/login.component';
import { UserLayoutComponent } from '../user-layout/user-layout.component';
import { MaterialModule } from '../material/material.module';

import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [LoginComponent, UserLayoutComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    HttpClientModule
  ]
})
export class UserModule { }
