import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AppStorage } from '../app-storage';


@Injectable()

export class CanActivateToken implements CanActivate {
   constructor(private router:Router){

   }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        if (!AppStorage.isToken()) {           
            this.router.navigate(['login']);            
        }
        return AppStorage.isToken();
    }
}
