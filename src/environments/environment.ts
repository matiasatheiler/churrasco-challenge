// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  url:'http://churrasco.uk.to:3005/api/',
  production: false,
  mapBoxKey:'pk.eyJ1IjoibWF0aWFzYXRoZWlsZXIiLCJhIjoiY2tkZHZwczB1MDEwZDJ6dDFjOXBwZnQ1NCJ9.GFVDnVHN3Q_oMeCDykrjdQ'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
